/**
 * Application entry point
 *
 * @author Zach Jones
 * @version 2-14-2017
 */

// Standard library imports
//TODO: clean these up so we only use what we need
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;

// OpenCV imports
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractor;
import org.opencv.video.Video;
import org.opencv.videoio.*;

//
// Detects faces in an image, draws boxes around them, and displays the result in a JFrame
class DetectFaceDemo {
    public void run() {
        System.out.println("\nRunning Video Demo");

        // get current screen resolution
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();

        // JFrame to show the original video
        JFrame original = new JFrame();
        original.setLayout(new FlowLayout());
        original.setTitle("Original");
        JLabel lbl = new JLabel();
        original.add(lbl);
        original.setVisible(true);
        original.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // JFrame to show the transformed video
        JFrame transformed = new JFrame();
        transformed.setLayout(new FlowLayout());
        transformed.setTitle("Transformed");
        JLabel transformedLabel = new JLabel();
        transformed.add(transformedLabel);
        transformed.setVisible(true);
        transformed.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        VideoCapture video = new VideoCapture();
//        int fourcc = VideoWriter.fourcc('M','P','4','2');
//        video.set(Videoio.CAP_PROP_FOURCC, fourcc);
//        video.open("rassegna2.avi");
        video.open("C:\\Users\\Zach Jones\\Downloads\\half_speed_60fps_sunny.mp4");
        video.set(Videoio.CAP_PROP_FPS, 60);

        //set the size of the frame to match the video source
//        original.setSize((int)video.get(Videoio.CAP_PROP_FRAME_WIDTH) + 50, (int)video.get(Videoio.CAP_PROP_FRAME_HEIGHT) + 50);
        double originalAspectRatio = video.get(Videoio.CAP_PROP_FRAME_WIDTH) / video.get(Videoio.CAP_PROP_FRAME_HEIGHT);
        original.setSize((int)(width*0.5), (int)(width*0.5/originalAspectRatio));
        transformed.setSize((int)(width*0.5), (int)(width*0.5/originalAspectRatio));

        boolean hasFrame = video.grab();
        Mat currentFrame = new Mat();
        Mat transformedFrame = new Mat();
        BufferedImage img;
        byte[] data;
        ImageIcon icon;

        // Image processing algorithms:
        BackgroundSubtractor bgSubtractor = Video.createBackgroundSubtractorMOG2();

        while(hasFrame){
            video.retrieve(currentFrame); // read data into current frame
            Imgproc.resize(currentFrame, currentFrame, new Size(width * 0.5, width*0.5/originalAspectRatio));

            img = new BufferedImage(currentFrame.width(), currentFrame.height(), BufferedImage.TYPE_3BYTE_BGR);
            // Get the BufferedImage's backing array and copy the pixels directly into it
            data = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
            currentFrame.get(0, 0, data);
            icon = new ImageIcon(img);
            lbl.setIcon(icon);

            // do some transformations to the current frame
            bgSubtractor.apply(currentFrame, transformedFrame, 0.1);
            transformedFrame.get(0, 0, data);
            icon = new ImageIcon(img);
            transformedLabel.setIcon(icon);

            // wait 1/60th of a second
//            try{
//                Thread.sleep(16);
//            } catch(InterruptedException ex){
//            }

            // get the next frame from the video
            hasFrame = video.grab();
        }

        System.out.println("Done showing video!");
        original.setVisible(false);
        original.dispose();
        transformed.setVisible(false);
        transformed.dispose();
        System.exit(0);
    }
}

public class Main {
    public static void main(String[] args) {
        // Load the native library.
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        new DetectFaceDemo().run();
    }
}
